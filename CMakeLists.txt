# CMake script for C++ FastaManager
# Author: Olivier Langella
# Created: 05/06/2015

# Global parameters
CMAKE_MINIMUM_REQUIRED(VERSION 2.6)
PROJECT(fastamanager CXX)
IF(NOT CMAKE_BUILD_TYPE)
#  SET(CMAKE_BUILD_TYPE RelWithDebInfo CACHE STRING
#      "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel."
#      FORCE)
  SET(CMAKE_BUILD_TYPE Debug CACHE STRING
      "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel."
      FORCE)
ENDIF(NOT CMAKE_BUILD_TYPE)

MESSAGE("CMAKE_BUILD_TYPE : ${CMAKE_BUILD_TYPE}")

IF(CMAKE_BUILD_TYPE MATCHES "Release")
  MESSAGE("compiling as release version")
  ADD_DEFINITIONS("-DQT_NO_DEBUG_OUTPUT")
ENDIF( CMAKE_BUILD_TYPE MATCHES "Release" )

IF(CMAKE_BUILD_TYPE MATCHES "Debug")
  MESSAGE("compiling as debug version")
ENDIF( CMAKE_BUILD_TYPE MATCHES "Debug" )

IF(CMAKE_BUILD_TYPE MATCHES "RelWithDebInfo")
  MESSAGE("compiling as release with debug info version")
ENDIF( CMAKE_BUILD_TYPE MATCHES "RelWithDebInfo" )

#SET(CMAKE_CXX_FLAGS "-Wall -Weffc++ -Wshadow -Wconversion")
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")



SET(SOFTWARE_NAME "FastaManager")

SET(FASTAMANAGER_VERSION_MAJOR "0")
SET(FASTAMANAGER_VERSION_MINOR "1")
SET(FASTAMANAGER_VERSION_PATCH "0")
SET(FASTAMANAGER_VERSION "${FASTAMANAGER_VERSION_MAJOR}.${FASTAMANAGER_VERSION_MINOR}.${FASTAMANAGER_VERSION_PATCH}")

# Set the CMAKE_PREFIX_PATH for the find_library fonction when using non
# standard install location
IF(CMAKE_INSTALL_PREFIX)
  SET(CMAKE_PREFIX_PATH "${CMAKE_INSTALL_PREFIX}" ${CMAKE_PREFIX_PATH})
ENDIF(CMAKE_INSTALL_PREFIX)

# Subdirectories
ADD_SUBDIRECTORY(src)

# Doxygen
FIND_PACKAGE(Doxygen)
IF (DOXYGEN_FOUND)
  ADD_CUSTOM_TARGET (apidoc cp Doxyfile ${CMAKE_BINARY_DIR}/Doxyfile-build
    COMMAND echo "OUTPUT_DIRECTORY=${CMAKE_BINARY_DIR}" >> ${CMAKE_BINARY_DIR}/Doxyfile-build
    COMMAND ${DOXYGEN_EXECUTABLE} ${CMAKE_BINARY_DIR}/Doxyfile-build
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})
  ADD_CUSTOM_TARGET (apidoc-stable cp Doxyfile ${CMAKE_BINARY_DIR}/Doxyfile-stable
    COMMAND echo "OUTPUT_DIRECTORY=${CMAKE_BINARY_DIR}" >> ${CMAKE_BINARY_DIR}/Doxyfile-stable
    COMMAND echo "HTML_HEADER=header.html" >> ${CMAKE_BINARY_DIR}/Doxyfile-stable
    COMMAND ${DOXYGEN_EXECUTABLE} ${CMAKE_BINARY_DIR}/Doxyfile-stable
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})
ENDIF (DOXYGEN_FOUND)

# Packager
SET(CPACK_PACKAGE_NAME "fastamanager")
SET(CPACK_PACKAGE_VENDOR "PAPPSO Development Team")
SET(CPACK_PACKAGE_VERSION "${FASTAMANAGER_VERSION}")
SET(CPACK_PACKAGE_VERSION_MAJOR "${FASTAMANAGER_VERSION_MAJOR}")
SET(CPACK_PACKAGE_VERSION_MINOR "${FASTAMANAGER_VERSION_MINOR}")
SET(CPACK_PACKAGE_VERSION_PATCH "${FASTAMANAGER_VERSION_PATCH}")
SET(CPACK_PACKAGE_DESCRIPTION_SUMMARY "fasta file manager")
SET(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_SOURCE_DIR}/COPYING")
SET(CPACK_RESOURCE_FILE_AUTHORS "${CMAKE_SOURCE_DIR}/AUTHORS")
SET(CPACK_RESOURCE_FILE_INSTALL "${CMAKE_SOURCE_DIR}/INSTALL")
SET(CPACK_SOURCE_GENERATOR "TGZ")
SET(CPACK_SOURCE_IGNORE_FILES
 "CMakeFiles"
 "Makefile"
 "_CPack_Packages"
 "CMakeCache.txt"
 ".*\\\\.git"
 ".*\\\\.gz"
 ".*\\\\.deb"
 ".*\\\\.rpm"
 ".*\\\\.dmg"
 ".*\\\\..*\\\\.swp"
 "src/\\\\..*"
 "src/libbpp*"
 "debian/tmp"
 "debian/libbpp.*/"
 "debian/libbpp.*\\\\.so.*"
 "debian/libbpp.*\\\\.a"
 "debian/libbpp.*\\\\.substvars"
 "debian/libbpp.*\\\\.debhelper"
 "debian/debhelper\\\\.log"
 "build/"
 "html"
 "Core.tag"
 "Testing"
 "build-stamp"
 "install_manifest.txt"
 "DartConfiguration.tcl"
 ${CPACK_SOURCE_IGNORE_FILES}
)



#dch -Djessie "message"
#cmake ..
#make deb
# scp pappsoms-tools* proteus.moulon.inra.fr:/var/www/apt/incoming
# reprepro -Vb /var/www/apt processincoming default
#
#  debuild -S -sa
# dput -f olivier-langella *changes

IF (MACOS)
  SET(CPACK_GENERATOR "Bundle")
ENDIF()

SET(CPACK_SOURCE_PACKAGE_FILE_NAME "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")
SET(CPACK_DEBSOURCE_PACKAGE_FILE_NAME "lib${CMAKE_PROJECT_NAME}_${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}.orig")
INCLUDE(CPack)

#This adds the 'dist' target
ADD_CUSTOM_TARGET(dist COMMAND ${CMAKE_MAKE_PROGRAM} package_source)
# 'clean' is not (yet) a first class target. However, we need to clean the directories before building the sources:
IF("${CMAKE_GENERATOR}" MATCHES "Make")
  ADD_CUSTOM_TARGET(make_clean
  COMMAND ${CMAKE_MAKE_PROGRAM} clean
  WORKING_DIRECTORY ${CMAKE_CURRENT_DIR}
  )
  ADD_DEPENDENCIES(dist make_clean)
ENDIF()

IF (UNIX)
#This creates deb packages:



	add_custom_target(targz
		cpack -G TGZ --config CPackSourceConfig.cmake && tar xvfz ${CPACK_PACKAGE_NAME}-${FASTAMANAGER_VERSION}.tar.gz
		WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
		COMMENT "Creating .tar.gz" VERBATIM
	)


	add_custom_target(deb
		cd ${CPACK_PACKAGE_NAME}-${FASTAMANAGER_VERSION} && dpkg-buildpackage
		DEPENDS targz
		WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
		COMMENT "Creating Debian package" VERBATIM
	)

	file(GLOB changesdebs "${CMAKE_BINARY_DIR}/${CPACK_PACKAGE_NAME}_*.dsc")

	foreach(libfile ${changesdebs})
	    SET(changesdeb "${libfile}")
	endforeach(libfile)

	message ("changes debian file : ${changesdeb}")
	#lintian -IEi --pedantic tandem-mass_2013.06.15-1_amd64.changes
	add_custom_target(lintian
		lintian -IEi --pedantic ${changesdeb}
		DEPENDS deb
		WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
		COMMENT "lintian check" VERBATIM
	)

ENDIF()

SET(CTEST_UPDATE_TYPE git)
SET(UPDATE_COMMAND "git")
SET(UPDATE_OPTIONS "")

#ENABLE_TESTING()
#INCLUDE(CTest)
#IF (BUILD_TESTING)
#  ADD_SUBDIRECTORY(test)
#ENDIF(BUILD_TESTING)

INSTALL(PROGRAMS ${CMAKE_BINARY_DIR}/src/fastamanager DESTINATION bin)