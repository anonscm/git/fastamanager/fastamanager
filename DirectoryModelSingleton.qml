pragma Singleton
import QtQuick 2.0

ListModel{
    property QtObject selection
    ListElement{
        directoryName: "database"
        fullPath: "/home/thierry/database"
        nbDatabase: "42"
    }
    ListElement{
        directoryName: "database"
        fullPath: "/gorgone/pappso/moulon/database"
        nbDatabase: "142"
    }
}
