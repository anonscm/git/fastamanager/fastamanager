import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1

TableView {
    id: directoryTable
    anchors.leftMargin: 12
    visible: true
    anchors.bottomMargin: 12
    anchors.bottom: parent.bottom
    anchors.right: parent.right

    TableViewColumn {
        role: "directoryName"
        title: qsTr("Directory Name")
        width: splitView1.width / 6
    }

    TableViewColumn {
        role: "fullPath"
        title: qsTr("Directory fullpath")
        width: splitView1.width / 6
    }

    TableViewColumn {
        role: "nbDatabase"
        title: qsTr("Number of database")
        width: splitView1.width / 6
    }
}
