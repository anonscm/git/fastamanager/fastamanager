import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1

TableView {
    id: listTable

    TableViewColumn {
        role: "name"
        title: qsTr("Database Name")
        width: splitView1.width / 6
    }

    TableViewColumn {
        role: "organism"
        title: qsTr("Organism")
        width: splitView1.width / 6
    }

    TableViewColumn {
        role: "directory"
        title: qsTr("Directory")
        width: splitView1.width / 6
    }
}
