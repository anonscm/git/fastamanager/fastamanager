import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0

Item {
    id: mainView
    property alias directoryTable: directoryTable

    SplitView {
        id: splitView1
        anchors.fill: parent

        ListTable {
            id: listTable
            Layout.minimumWidth: splitView1.width / 2 - 1
        }

        ColumnLayout {
            id: columnLayout1
            width: 300
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.right: parent.right

            GridLayout {
                id: gridLayout1
                Layout.fillWidth: false
                Layout.minimumWidth: 200
                Layout.minimumHeight: 300
                visible: true
                rows: 12
                columns: 3
                anchors.top: parent.top
                anchors.right: parent.right

                Text {
                    id: researchSection
                    text: qsTr("Search")
                    Layout.fillWidth: true
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    Layout.columnSpan: 3
                    font.pixelSize: 12
                }

                ComboBox {
                    id: comboBox1
                }

                TextField {
                    id: textField1
                    readOnly: false
                    placeholderText: qsTr("Text Field")
                }

                Text {
                    id: detailsSection
                    text: qsTr("Details")
                    Layout.fillWidth: true
                    horizontalAlignment: Text.AlignHCenter
                    Layout.columnSpan: 3
                    font.pixelSize: 12
                }

                Text {
                    id: nameLabel
                    text: qsTr("Name")
                    font.pixelSize: 12
                }

                TextField {
                    id: nameValue
                    readOnly: true
                    Layout.fillWidth: false
                    Layout.columnSpan: 2
                    placeholderText: qsTr("Text Field")
                }

                Text {
                    id: privateLabel
                    text: qsTr("Private")
                    font.pixelSize: 12
                }

                CheckBox {
                    id: privateValue
                    text: qsTr("")
                    Layout.columnSpan: 2
                }

                Text {
                    id: organismLabel
                    text: qsTr("Organism")
                    font.pixelSize: 12
                }

                TextField {
                    id: organismValue
                    readOnly: true
                    Layout.fillWidth: false
                    Layout.columnSpan: 2
                    placeholderText: qsTr("Text Field")
                }

                Text {
                    id: nbseqLabel
                    text: qsTr("Number of entries")
                    font.pixelSize: 12
                }

                TextField {
                    id: nbseqValue
                    readOnly: true
                    Layout.fillWidth: false
                    Layout.columnSpan: 2
                    placeholderText: qsTr("Text Field")
                }

                Text {
                    id: directoryLabel
                    text: qsTr("Directory")
                    font.pixelSize: 12
                }

                TextField {
                    id: directoryValue
                    readOnly: true
                    Layout.columnSpan: 2
                    placeholderText: qsTr("Text Field")
                }

                Text {
                    id: sourceLabel
                    text: qsTr("Source")
                    font.pixelSize: 12
                }

                TextField {
                    id: sourceValue
                    readOnly: true
                    Layout.columnSpan: 2
                    placeholderText: qsTr("Text Field")
                }

                Text {
                    id: urlLabel
                    text: qsTr("Url")
                    font.pixelSize: 12
                }

                TextField {
                    id: urlValue
                    readOnly: true
                    Layout.columnSpan: 2
                    placeholderText: qsTr("Text Field")
                }

                Text {
                    id: transformationLabel
                    text: qsTr("Transformation")
                    font.pixelSize: 12
                }

                TextField {
                    id: transformationValue
                    readOnly: true
                    Layout.columnSpan: 2
                    placeholderText: qsTr("Text Field")
                }

                Text {
                    id: seqLabel
                    text: qsTr("Sequence type")
                    font.pixelSize: 12
                }

                TextField {
                    id: seqValue
                    readOnly: true
                    Layout.columnSpan: 2
                    placeholderText: qsTr("Text Field")
                }

                Text {
                    id: versionLabel
                    text: qsTr("Version")
                    font.pixelSize: 12
                }

                TextField {
                    id: versionValue
                    readOnly: true
                    Layout.columnSpan: 2
                    placeholderText: qsTr("Text Field")
                }

                Text {
                    id: dateLabel
                    text: qsTr("Creation date")
                    font.pixelSize: 12
                }

                TextField {
                    id: dateValue
                    readOnly: true
                    Layout.columnSpan: 2
                    placeholderText: qsTr("Text Field")
                }

                Text {
                    id: descriptionLabel
                    text: qsTr("Description")
                    font.pixelSize: 12
                }

                TextArea {
                    id: descriptionValue
                    width: 200
                    readOnly: true
                    Layout.fillWidth: true
                }
            }

            DirectoryTable {
                id: directoryTable
                Layout.fillWidth: true
            }
        }
    }
}
