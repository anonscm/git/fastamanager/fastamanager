#ifndef FASTADB_H
#define FASTADB_H
#include <QMap>
#include "fastafile.h"
#include <QString>

class FastaDB
{
public:
    FastaDB();
    void addFastaFile(FastaFile myDB);
    int countDB();

private:
    QMap<QString, FastaFile> fastafiles;

};

#endif // FASTADB_H
