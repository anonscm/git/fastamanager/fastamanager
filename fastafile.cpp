#include "fastafile.h"

FastaFile::FastaFile()
{}

void FastaFile::addInfo(QString fastaname, QString isprivate, QString dbfullpath, QString organism, QString source, QString url, QString transformation, QString seqtype, QString version, QString creationdate, QString description, QString nbseq){
    this->fastaname = fastaname;
    this->isprivate = isprivate;
    this->dbfullpath = dbfullpath;
    this->organism = organism;
    this->source = source;
    this->url = url;
    this->transformation = transformation;
    this->seqtype = seqtype;
    this->version = version;
    this->creationdate = creationdate;
    this->description = description;
    this->nbseq = nbseq;
}

QString FastaFile::getDbfullpath(){
    return this->dbfullpath;
}
