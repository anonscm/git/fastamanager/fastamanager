#ifndef FASTAFILE_H
#define FASTAFILE_H

#include <QString>

class FastaFile
{
public:
    FastaFile();
    FastaFile(QString fastaname,QString isprivate,QString dbfullpath,QString organism,QString source,QString url,QString transformation,QString seqtype,QString version,QString creationdate,QString description,QString nbseq);

    void addInfo(QString fastaname,QString isprivate,QString dbfullpath,QString organism,QString source,QString url,QString transformation,QString seqtype,QString version,QString creationdate,QString description,QString nbseq);



    QString getFastaname();
    QString getPrivateStatue();
    QString getDbfullpath();
    QString getOrganism();
    QString getSource();
    QString getUrl();
    QString getTransformation();
    QString getSeqtype();
    QString getVersion();
    QString getCreationdate();
    QString getDescription();
    QString getNbseq();

private:

    QString fastaname;
    QString isprivate;
    QString dbfullpath;
    QString organism;
    QString source;
    QString url;
    QString transformation;
    QString seqtype;
    QString creationdate;
    QString description;
    QString nbseq;
    QString version;
};

#endif // FASTAFILE_H
