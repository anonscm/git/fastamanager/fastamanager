#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QUrl resourceUrl(QStringLiteral("qrc:/DirectoryModelSingleton.qml"));
    qmlRegisterSingletonType(resourceUrl, "my.directorymodel.singleton", 1, 0, "DirectoryModel");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
