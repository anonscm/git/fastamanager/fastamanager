import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import my.directorymodel.singleton 1.0

ApplicationWindow {
    visible: true
    title: qsTr("pt-FastaManager")

    menuBar: MenuBar {
        Menu {
            title: qsTr("File")
            MenuItem {
                text: qsTr("&Open")
                onTriggered: console.log("Open action triggered");
            }
            MenuItem {
                text: qsTr("Exit")
                onTriggered: Qt.quit();
            }
        }
    }

    MainForm {
        anchors.fill: parent
        Layout.minimumWidth: 1024
        Layout.minimumHeight: 780
        Layout.preferredWidth: 1012
        Layout.preferredHeight: 780
        directoryTable.model: DirectoryModel
        Component.onCompleted: DirectoryModel.selection = directoryTable.selection
    }

    MessageDialog {
        id: messageDialog
        title: qsTr("May I have your attention, please?")

        function show(caption) {
            messageDialog.text = caption;
            messageDialog.open();
        }
    }
}
