
/*******************************************************************************
* Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
*
* This file is part of XTPcpp.
*
*     XTPcpp is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     XTPcpp is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
*
* Contributors:
*     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
******************************************************************************/

#include "proteinlistwindow.h"

#include "ui_protein_view.h"
#include "proteintablemodel.h"


ProteinListWindow::ProteinListWindow(QWidget *parent):
    QMainWindow(parent),
    ui(new Ui::ProteinView)
{
    ui->setupUi(this);

#if QT_VERSION >= 0x050000
    // Qt5 code
    /*
    connect(&workerThread, &QThread::finished, worker, &QObject::deleteLater);
    connect(this, &PtSpectrumViewer::operateMsDataFile, worker, &PwizLoaderThread::doMsDataFileLoad);
    connect(worker, &PwizLoaderThread::msDataReady, this, &PtSpectrumViewer::handleMsDataFile);
*/
#else
// Qt4 code
/*
    connect(&workerThread, SIGNAL(finished()), worker, SLOT(deleteLater()));
    connect(this, SIGNAL(operateMsDataFile(QString)), worker,SLOT(doMsDataFileLoad(QString)));
    connect(worker, SIGNAL(msDataReady(pwiz::msdata::MSDataFile *)), this, SLOT(handleMsDataFile(pwiz::msdata::MSDataFile *)));
    */
#endif
    /*
    */

    ProteinTableModel * p_myModel = new ProteinTableModel(0);
    ui->tableView->setModel( p_myModel );
}

ProteinListWindow::~ProteinListWindow()
{
    //if (_p_ms_data_file != nullptr) delete _p_ms_data_file;
    delete ui;

}