
/*******************************************************************************
* Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
*
* This file is part of XTPcpp.
*
*     XTPcpp is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     XTPcpp is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
*
* Contributors:
*     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
******************************************************************************/

#ifndef PROTEINLISTWINDOW_H
#define PROTEINLISTWINDOW_H


#include <QMainWindow>

//http://doc.qt.io/qt-4.8/qt-itemviews-chart-mainwindow-cpp.html
namespace Ui {
class ProteinView;
}

class ProteinListWindow: public QMainWindow {
    Q_OBJECT

public:

    explicit ProteinListWindow(QWidget * parent = 0);
    ~ProteinListWindow();

public slots:
    //void peptideEdited(QString peptideStr);
    // void setColor(const QColor &color);
    // void setShape(Shape shape);
signals:
    //void peptideChanged(pappso::PeptideSp peptide);

private:
    Ui::ProteinView *ui;

};

#endif // PROTEINLISTWINDOW_H
