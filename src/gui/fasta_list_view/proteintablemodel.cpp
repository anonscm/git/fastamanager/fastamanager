
/*******************************************************************************
* Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
*
* This file is part of XTPcpp.
*
*     XTPcpp is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     XTPcpp is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
*
* Contributors:
*     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
******************************************************************************/

#include "proteintablemodel.h"

#include <QDebug>

ProteinTableModel::ProteinTableModel(QObject *parent)
    :QAbstractTableModel(parent)
{

    //ui->tableView->show();
    // QModelIndex topLeft = createIndex(0,0);
    //emit a signal to make the view reread identified data
    //emit dataChanged(topLeft, topLeft);
}

int ProteinTableModel::rowCount(const QModelIndex &parent ) const {
    return 2;
}
int ProteinTableModel::columnCount(const QModelIndex &parent ) const {
    return 3;
}
QVariant ProteinTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if (orientation == Qt::Horizontal) {
            switch (section)
            {
            case 0:
                return QString("first");
            case 1:
                return QString("second");
            case 2:
                return QString("third");
            }
        }
    }
    return QVariant();
}
QVariant ProteinTableModel::data(const QModelIndex &index, int role ) const {
    // generate a log message when this method gets called
    int row = index.row();
    int col = index.column();
    qDebug() << QString("row %1, col%2, role %3")
             .arg(row).arg(col).arg(role);
    if (role == Qt::DisplayRole)
    {
        return QString("Row%1, Column%2")
               .arg(index.row() + 1)
               .arg(index.column() +1);
    }
    return QVariant();
}
