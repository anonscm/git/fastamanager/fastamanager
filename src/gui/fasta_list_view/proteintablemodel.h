
/*******************************************************************************
* Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
*
* This file is part of XTPcpp.
*
*     XTPcpp is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     XTPcpp is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
*
* Contributors:
*     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
******************************************************************************/

#ifndef PROTEINTABLEMODEL_H
#define PROTEINTABLEMODEL_H

#include <QAbstractTableModel>

class ProteinTableModel: public QAbstractTableModel
{
    Q_OBJECT
public:
    ProteinTableModel(QObject *parent=0);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
};

#endif // PROTEINTABLEMODEL_H
