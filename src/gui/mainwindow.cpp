
/*******************************************************************************
* Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
*
* This file is part of XTPcpp.
*
*     XTPcpp is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     XTPcpp is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
*
* Contributors:
*     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
******************************************************************************/

#include <QDockWidget>
#include <QSortFilterProxyModel>
#include "mainwindow.h"
#include "fasta_list_view/fastatablemodel.h"

#include "ui_main.h"


MainWindow::MainWindow(QWidget *parent):
    QMainWindow(parent),
    ui(new Ui::Main)
{
    ui->setupUi(this);

    PwizLoaderThread *worker = new PwizLoaderThread;
    worker->moveToThread(&workerThread);
#if QT_VERSION >= 0x050000
    // Qt5 code
    /*
    connect(&workerThread, &QThread::finished, worker, &QObject::deleteLater);
    connect(this, &PtSpectrumViewer::operateMsDataFile, worker, &PwizLoaderThread::doMsDataFileLoad);
    connect(worker, &PwizLoaderThread::msDataReady, this, &PtSpectrumViewer::handleMsDataFile);
    */
#else
// Qt4 code
    /*
        connect(&workerThread, SIGNAL(finished()), worker, SLOT(deleteLater()));
        connect(this, SIGNAL(operateMsDataFile(QString)), worker,SLOT(doMsDataFileLoad(QString)));
        connect(worker, SIGNAL(msDataReady(pwiz::msdata::MSDataFile *)), this, SLOT(handleMsDataFile(pwiz::msdata::MSDataFile *)));
        */
#endif
    /*
    */
    workerThread.start();
    
    
    FastaTableModel * p_myModel = new FastaTableModel(0);
    p_myModel->setPath("./");
    QSortFilterProxyModel *proxyModel = new QSortFilterProxyModel(p_myModel);
    ui->fastaTableView->setModel( proxyModel );
/*
     QDockWidget *dock = new QDockWidget(tr("Protein List"), this);
    _protein_list_window = new ProteinListWindow(this);
    //_protein_list_window->show();
    dock->setWidget(_protein_list_window);
    addDockWidget(Qt::RightDockWidgetArea, dock);
    */
}

MainWindow::~MainWindow()
{
    workerThread.quit();
    workerThread.wait();
    //if (_p_ms_data_file != nullptr) delete _p_ms_data_file;
    delete ui;

}
