
/*******************************************************************************
* Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
*
* This file is part of XTPcpp.
*
*     XTPcpp is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     XTPcpp is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
*
* Contributors:
*     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
******************************************************************************/


#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include <QFileInfo>
#include <pappsomspp/types.h>
#include <pappsomspp/peptide/peptide.h>
#include <pappsomspp/spectrum/spectrum.h>



namespace Ui {
class Main;
}


class PwizLoaderThread : public QObject
{
    Q_OBJECT
    public:

public slots:
    void doMsDataFileLoad(const QString & parameter) {
        /* ... here is the expensive or blocking operation ... */
        //emit msDataReady(p_ms_data_file);
    }

signals:
    //void msDataReady(pwiz::msdata::MSDataFile * p_ms_data_file);
};

class MainWindow: public QMainWindow {	
    Q_OBJECT
    QThread workerThread;

public:

    explicit MainWindow(QWidget * parent = 0);
    ~MainWindow();

public slots:
    //void peptideEdited(QString peptideStr);
    // void setColor(const QColor &color);
    // void setShape(Shape shape);
signals:
    //void peptideChanged(pappso::PeptideSp peptide);

private:
    Ui::Main *ui;
    
    
   // ProteinListWindow * _protein_list_window=nullptr;

};

#endif // MAINWINDOW_H
