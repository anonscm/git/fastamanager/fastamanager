#include "xmlfastareader.h"

#include <QFile>
#include <QDebug>

XmlFastaReader::XmlFastaReader(const QString filename, FastaDB &dblist):
    _filename(filename), dblist(dblist)
{}

void XmlFastaReader::read(){
    QFile xmlFile(_filename);
    xmlFile.open(QIODevice::ReadOnly);
    xml.setDevice(&xmlFile);

    if (xml.readNextStartElement() && xml.name() == "database")
        this->dblist.addFastaFile(processfastaxml());
    if(xml.tokenType() == QXmlStreamReader::Invalid)
        xml.readNext();

    if (xml.hasError()){
        xml.raiseError();
        qDebug() << errorString();
    }
}
FastaFile XmlFastaReader::processfastaxml(){
    FastaFile myDb;
    if (!xml.isStartElement() || xml.name() != "database")
        return myDb;

    QString fastaname;
    QString isprivate;
    QString dbfullpath;
    QString organism;
    QString source;
    QString url;
    QString transformation;
    QString seqtype;
    QString version;
    QString creationdate;
    QString description;
    QString nbqseq;
    int count = 0;

    while (xml.readNextStartElement()){
        if (xml.name() == "fastaname"){
            fastaname = readNextText();
            count ++;
        }
        else if (xml.name() == "isprivate"){
            isprivate = readNextText();
            count ++;
        }
        else if (xml.name() == "dbfullpath"){
            dbfullpath = readNextText();
            count ++;
        }
        else if (xml.name() == "organism"){
            organism = readNextText();
            count ++;
        }
        else if (xml.name() == "source"){
            source = readNextText();
            count ++;
        }
        else if (xml.name() == "url"){
            url = readNextText();
            count ++;
        }
        else if (xml.name() == "transformation"){
            transformation = readNextText();
            count ++;
        }
        else if (xml.name() == "seqtype"){
            seqtype = readNextText();
            count ++;
        }
        else if (xml.name() == "version"){
            version = readNextText();
            count ++;
        }
        else if (xml.name() == "creationdate"){
            creationdate = readNextText();
            count ++;
        }
        else if (xml.name() == "description"){
            description = readNextText();
            count ++;
        }
        else if (xml.name() == "nbqseq"){
            nbqseq = readNextText();
            count ++;
        }
#ifndef USE_READ_ELEMENT_TEXT
        xml.skipCurrentElement();
#endif
    }

    if(count == 12){
        myDb.addInfo(fastaname, isprivate, dbfullpath, organism, source, url, transformation, seqtype, version, creationdate, description, nbqseq);
        return myDb;
       }else{
        return myDb;
    }
}

QString XmlFastaReader::readNextText() {
#ifndef USE_READ_ELEMENT_TEXT
    xml.readNext();
    return xml.text().toString();
#else
    return xml.readElementText();
#endif
}

QString XmlFastaReader::errorString() {
    return QObject::tr("%1\nLine %2, column %3")
            .arg(xml.errorString())
            .arg(xml.lineNumber())
            .arg(xml.columnNumber());
}

