#ifndef XMLFASTAREADER_H
#define XMLFASTAREADER_H

#include <QXmlStreamReader>
#include <QString>
#include "fastafile.h"
#include "fastadb.h"

class XmlFastaReader
{
public:
    XmlFastaReader(const QString filename, FastaDB &dblist);

    void read();

private:
    FastaFile processfastaxml();
    QString readNextText();
    QString errorString();

    QString _filename;
    QXmlStreamReader xml;
    FastaDB & dblist;
};

#endif // XMLFASTAREADER_H
