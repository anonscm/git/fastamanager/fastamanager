#ifndef XMLFASTAWRITER_H
#define XMLFASTAWRITER_H

#include <fastafile.h>
#include <QXmlStreamWriter>
#include <QString>

class XmlFastaWriter
{
public:
    XmlFastaWriter();

    void setFastaFile(FastaFile & fastafile);

    void writeFastaOutput();

private:
    FastaFile fastafile;

};

#endif // XMLFASTAWRITER_H
